LensMC - Weak lensing shear measurement with forward modelling and MCMC sampling
===================================

.. warning::

   This documentation is currently under construction and subject to change.

LensMC is a Python package for weak lensing shear measurement specifically developed for Euclid and stage-IV weak lensing surveys.
It is based on forward modelling and fast MCMC sampling.
To acknowledge this work, please cite the following paper: `Euclid Collaboration: G. Congedo et al., arXiv:2405.00669 (2024) <https://arxiv.org/abs/2405.00669>`_

Project repository: `gitlab.com/gcongedo/LensMC <https://gitlab.com/gcongedo/LensMC>`_

Contact: <giuseppe.congedo@ed.ac.uk>
