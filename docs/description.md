## LensMC - Weak lensing shear measurement with forward modelling and MCMC sampling

LensMC is a Python package for weak lensing shear measurement specifically developed for Euclid and stage-IV weak lensing surveys.
It is based on forward modelling and fast MCMC sampling.
To acknowledge this work, please cite the following paper: [Euclid Collaboration: G. Congedo et al., arXiv:2405.00669 (2024)](https://arxiv.org/abs/2405.00669).

Contact: <giuseppe.congedo@ed.ac.uk>

## License

GNU Lesser General Public License - Copyright (C) 2015 Giuseppe Congedo, University of Edinburgh on behalf of the Euclid Science Ground Segment
