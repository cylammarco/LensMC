# Makefile for LensMC python package
#
# make: build environment and dependencies
# make install: install LensMC
# make install-inplace: install in place
# make dist: build distribution source and wheel
# make test: run tests
# make purge: full clean up of the directory
# make clean just clean up install directories
# make clean: clean up build and install directories
#
# make CC=icc LDSHARED="icc -shared": build with Intel compiler
# make CC=icc LDSHARED="icc -shared" install: install with Intel compiler
#
# Copyright 2015 Giuseppe Congedo

SHELL := /bin/bash

PREFIX=lensmc-env
ENV=$(shell basename $(PREFIX))
CONDA_INSTALLER=Miniconda3-py39_23.1.0-1-Linux-x86_64.sh
CONDA_ACTIVATE=source conda/miniconda3/etc/profile.d/conda.sh ; conda activate; conda activate

all:
	@$(MAKE) conda

	@$(MAKE) environment

	@$(MAKE) environment-installer

	@echo Installing environment...

	bash conda/$(ENV)-1.0.0-Linux-x86_64.sh
	@source $(PREFIX)/bin/activate && conda config --set env_prompt '({name}) '

	@echo '#'
	@echo '#' To activate this environment, use
	@echo '#'
	@echo '#'     \$ source activate $(PREFIX)
	@echo '#'
	@echo '#' To deactivate it, use
	@echo '#'
	@echo '#'     $ conda deactivate
	@echo

conda:
	@echo Downloading $(CONDA_INSTALLER)...
	@wget https://repo.anaconda.com/miniconda/$(CONDA_INSTALLER) -P conda
	@echo Installing $(CONDA_INSTALLER)...
	@bash conda/$(CONDA_INSTALLER) -b -p conda/miniconda3

environment:
	@echo Creating environment...
	$(CONDA_ACTIVATE) && bash -c "conda env create -n $(ENV) -f environment.yml --force"

environment-installer:
	@echo Building environment installer...
	$(CONDA_ACTIVATE) $(ENV) && constructor --platform=linux-64 --output-dir=conda .

install:
	source $(PREFIX)/bin/activate && python setup.py install --force

install-inplace:
	source $(PREFIX)/bin/activate && python setup.py build_ext --inplace --force

dist:
	source $(PREFIX)/bin/activate && python -m build --sdist

pypi:
	source $(PREFIX)/bin/activate && twine check dist/*.tar.gz && twine upload dist/*.tar.gz -u __token__ --verbose

test:
	source $(PREFIX)/bin/activate && python setup.py build_ext --inplace --force
	source $(PREFIX)/bin/activate && cd lensmc/tests && python -m pytest -v --log-cli-level=INFO -s --capture=no test*.py
	@rm -fr lensmc/*.so lensmc/src/cython/*.c*
	@rm -fr lensmc/__pycache__

purge:
	@$(MAKE) clean
	@rm -fr $(PREFIX) conda

clean:
	@rm -fr $(PREFIX)/lib/python*/site-packages/lensmc* build dist lensmc.egg-info lensmc/INFO
	@rm -fr lensmc/*.so lensmc/src/cython/*.c*
	@rm -fr lensmc/__pycache__ __pycache__
	@find lensmc -name '*.pyc' -type f -delete
